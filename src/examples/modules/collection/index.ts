import { CollectionModule } from '@reachu/sdk';

const collectionModule = new CollectionModule.Core.Management(
  '9FCT52V-HMD4AFY-NQFQ4X9-56TSKGC',
  'https://api-qa.outshifter.com',
);

//Module Base
(async () => {
  //----- Init Create Base(Collection)-----
  const sendCreateData: CollectionModule.Type.Base.SendCreateService = {
    title: 'Test Collection',
    description: 'This is just a test collection',
    slug: 'TestCollection',
    image: '',
  };
  const responseCreateCollection = await collectionModule.base.create(sendCreateData);
  console.info('Create Base(Collection) =>', responseCreateCollection);
  //----- Finish Create Base(Collection)-----

  //----- Init Update Base(Collection)-----
  const sendUpdateData: CollectionModule.Type.Base.SendUpdateService = {
    collection_id: responseCreateCollection.id,
    title: 'Test Collection update',
    description: 'This is just a test collection update',
    slug: 'TestCollection update',
    image: '',
  };
  const responseUpdateCollection = await collectionModule.base.update(sendUpdateData);
  console.info('Update Base(Collection) =>', responseUpdateCollection);
  //----- Finish Update Base(Collection)-----

  //----- Init GetById Base(Collection)-----
  const responseGetByIdCollection = await collectionModule.base.getById(
    responseCreateCollection.id,
  );
  console.info('GetById Base(Collection) =>', responseGetByIdCollection);
  //----- Finish GetById Base(Collection)-----

  //----- Init getByUser Base(Collection)-----
  const responseGetByUserCollection = await collectionModule.base.getByUser();
  console.info('getByUser Base(Collection) =>', responseGetByUserCollection);
  //----- Finish getByUser Base(Collection)-----

  //----- Init getByUserId Base(Collection)-----
  const responseGetByUserIdCollection = await collectionModule.base.getByUserId(
    responseCreateCollection.user.id,
  );
  console.info('getByUserId Base(Collection) =>', responseGetByUserIdCollection);
  //----- Finish getByUserId Base(Collection)-----

  //----- Init Delete Base(Collection)-----
  const sendDeleteData: CollectionModule.Type.Base.SendDeleteService = {
    collection_id: responseCreateCollection.id,
  };
  const responseCollection = await collectionModule.base.delete(sendDeleteData);
  console.info('Delete Base(Collection) =>', responseCollection);
  //----- Finish Delete Base(Collection)-----
})();

//Module Item
(async () => {
  const sendCreateCollection: CollectionModule.Type.Base.SendCreateService = {
    title: 'Test Collection',
    description: 'This is just a test collection',
    slug: 'TestCollection',
    image: '',
  };
  const responseCreateCollection = await collectionModule.base.create(sendCreateCollection);

  //----- Init Create Item-----
  const sendCreateItem: CollectionModule.Type.Item.SendCreateService = {
    collection_id: responseCreateCollection.id,
    product_id: 5,
  };
  const responseCreateItem = await collectionModule.item.create(sendCreateItem);
  console.info('Create Item =>', responseCreateItem);
  //----- Finish Create Item-----

  //----- Init Delete Item-----
  const sendDeleteItem = {
    collection_id: responseCreateCollection.id,
    product_id: 5,
  };
  const responseDeleteItem = await collectionModule.item.delete(sendDeleteItem);
  console.info('Delete Item =>', responseDeleteItem);
  //----- Finish Delete Item-----
})();

//Module Shared
(async () => {
  const sendCreateDataCollection: CollectionModule.Type.Base.SendCreateService = {
    title: 'Test Collection',
    description: 'This is just a test collection',
    slug: 'TestCollection',
    image: '',
  };
  const responseCreateCollection = await collectionModule.base.create(sendCreateDataCollection);
  const sendCreateItem: CollectionModule.Type.Item.SendCreateService = {
    collection_id: responseCreateCollection.id,
    product_id: 5,
  };
  await collectionModule.item.create(sendCreateItem);

  //----- Init Create Shared-----
  const sendCreateShared: CollectionModule.Type.Shared.SendCreateService = {
    collection_id: responseCreateCollection.id,
    user_id: 546,
  };

  const responseCreateShared = await collectionModule.shared.create(sendCreateShared);
  console.info('Create Shared =>', responseCreateShared);
  //----- Finish Create Shared-----

  //----- Init getByUser Shared-----
  const responseGetByUserShared = await collectionModule.shared.getByUser();

  console.info('getByUser Shared =>', responseGetByUserShared);
  //----- Finish getByUser Shared-----

  //----- Init getByUserId Shared-----
  const responseGetByUserIdShared = await collectionModule.shared.getByUserId(546);

  console.info('getByUserId Shared =>', responseGetByUserIdShared);
  //----- Finish getByUserId Shared-----

  //----- Init Delete Shared-----
  const sendDeleteShared: CollectionModule.Type.Shared.SendDeleteService = {
    collection_id: responseCreateCollection.id,
    user_id: 546,
  };

  const responseDeleteShared = await collectionModule.shared.delete(sendDeleteShared);
  console.info('Delete Shared =>', responseDeleteShared);
  //----- Finish Delete Shared-----
})();
