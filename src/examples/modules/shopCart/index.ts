import { SdkClient } from '@reachu/sdk';

function sdk() {
  return new SdkClient('9FCT52V-HMD4AFY-NQFQ4X9-56TSKGC', 'https://api-qa.reachu.io');
}

(async () => {
  const client = sdk();
  const shopCartModule = client.shopCart;
  const discountModule = client.discount;

  let responseCreateCart;
  let responseDiscount;
  try {
    //----- Init Create Cart-----
    const sendCreateCart = {
      customer_session_id: 'testing_' + Math.floor(Math.random() * 1000),
      currency: 'NOK',
    };
    responseCreateCart = await shopCartModule.cart.create(sendCreateCart);
    //    console.info('create cart =>', responseCreateCart);
    //----- Finish Create Cart-----
  } catch (error) {
    console.error('An error occurred:', error.message);
  }

  try {
    //----- Init update by Id Cart-----
    const responseUpdateByIdCart = await shopCartModule.cart.update(responseCreateCart.cart_id, {
      shipping_country: 'NO',
    });
    //  console.info('update by id cart =>', responseUpdateByIdCart);
    //----- Finish update by Id Cart-----
  } catch (error) {
    console.error('An error occurred:', error);
  }

  try {
    //----- Init Create Cart Item-----
    const sendCreateCartItem = {
      cartId: responseCreateCart.cart_id,
      line_items: [
        {
          product_id: 6323,
          quantity: 1,
          price_data: {
            unit_price: 1000,
            tax: 20,
            currency: 'NOK',
          },
        },
        {
          product_id: 6322,
          variant_id: 13410,
          quantity: 1,
          price_data: {
            unit_price: 599.0,
            tax: 20,
            currency: 'NOK',
          },
        },
      ],
    };

    const responseCreateCartItem = await shopCartModule.cartItem.create(sendCreateCartItem);
    console.info('create cart item =>', responseCreateCartItem);
    //----- Finish Create Cart Item-----
  } catch (error) {
    console.error('Error creating cart item:', error.message);
  }

  try {
    const responseCreateCarts = await shopCartModule.cart.getById(responseCreateCart.cart_id);
    //  console.info('get by id cart =>', responseCreateCarts);
  } catch (error) {
    //  console.error('Error getting cart:', error.message);
  }
  //  Create Discount
  // console.log(`Creating Discount...`);
  try {
    responseDiscount = await discountModule.discount.addDiscount({
      code: `TEST_FROM_SDK_${Math.floor(Math.random() * 1000)}`,
      percentage: 10,
      typeId: 2,
      startDate: '2023-07-12T00:00:00.000-04:00',
      endDate: '2024-07-15T00:00:00.000-04:00',
    });
    //  console.log('Discount created successfully', responseDiscount);
  } catch (error) {
    console.error('Error creating discount:', error.message);
  }

  //  Create Discount
  // console.log(`Creating Discount...`);
  try {
    responseDiscount = await discountModule.discount.addDiscount({
      code: `TEST_FROM_SDK_${Math.floor(Math.random() * 1000)}`,
      percentage: 10,
      typeId: 1,
      startDate: '2023-07-12T00:00:00.000-04:00',
      endDate: '2024-07-15T00:00:00.000-04:00',
    });
    //  console.log('Discount created successfully', responseDiscount);
  } catch (error) {
    console.error('Error creating discount:', error.message);
  }

  //Get Discounts By Use
  console.log(`Getting Discounts codes availeble...`);
  try {
    const getDiscountUser = await discountModule.discount.discounts();
    //  console.log(getDiscountUser);
  } catch (error) {
    console.error('Error getting discount by user:', error.message);
  }

  //Verify Discount
  console.log(`Verifying Discount...${responseDiscount.code}`);
  try {
    const verifyDiscount = await discountModule.discount.verifyDiscount({
      code: responseDiscount.code,
    });
    //  console.log(`Verified Discount`, verifyDiscount);
  } catch (error) {
    console.error('Error verifying discount:', error.message);
  }

  //Apply Discount
  console.log(`Applying Discount...`);
  try {
    const appliedDiscount = await discountModule.discount.applyDiscount({
      code: responseDiscount.code,
      cartId: responseCreateCart.cart_id,
    });
    console.log(`   Discount applied to cart`, appliedDiscount);
  } catch (error) {
    console.error('Error applying discount:', error.message);
  }

  console.log(`Getting Cart...`);
  try {
    const responseCreateCarts = await shopCartModule.cart.getById(responseCreateCart.cart_id);
    console.info('get by id cart => line items', responseCreateCarts.line_items);
  } catch (error) {
    console.error('Error getting cart:', error.message);
  }
})();
