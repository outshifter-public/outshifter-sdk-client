//Create instance
import { ChannelModule } from '@reachu/sdk';

//Create instance con mi apikey de mi usuario general
const channelModule = new ChannelModule.Core.Management(
  'D48F61X-988MBZX-K31AWT3-HFV4XXZ',
  'https://api-staging.reachu.io',
);

(async () => {
  //----- Init getAllActive Base(Channel) traigo los channels activos -----
  const responseGetAllActiveChannel = await channelModule.base.getAllActive();
  console.info('getAllActive Base(Channel) =>', responseGetAllActiveChannel);
  //----- Finish getAllActive Base(Channel)-----

  //creo mi channel user especifico en este caso mi channel sera de SDK vs Wordpress los cuales van a tener configuraciones diferentes
  const channelWordpress = responseGetAllActiveChannel.channels.find(
    (channel) => channel.name === 'Wordpress',
  );
  const channelSdk = responseGetAllActiveChannel.channels.find((channel) => channel.name === 'Sdk');

  const sendCreateChannelUserWordpress: ChannelModule.Type.User.SendCreateService = {
    channel_id: channelWordpress.id,
    customName: `shoes shop ${Math.floor(Math.random() * Date.now())}`,
  };
  const responseCreateChannelUserWordpress = await channelModule.user.create(
    sendCreateChannelUserWordpress,
  );
  console.info('Create Channel-User-Wordpress  =>', responseCreateChannelUserWordpress);

  const sendCreateChannelUserSdk: ChannelModule.Type.User.SendCreateService = {
    channel_id: channelSdk.id,
    customName: `shirts shop ${Math.floor(Math.random() * Date.now())}`,
  };
  const responseCreateChannelUserSdk = await channelModule.user.create(sendCreateChannelUserSdk);
  console.info('Create Channel-User-Sdk  =>', responseCreateChannelUserSdk);

  const apiKeySdk = responseCreateChannelUserSdk.userChannelApiKey.apiKey;
  const apiKeyWordpress = responseCreateChannelUserWordpress.userChannelApiKey.apiKey;
  const userGeneralId = responseCreateChannelUserSdk.user.id;
  console.info('userGeneralId', userGeneralId);

  //Con esto me trae toda la info de todos los channels del usuario (cantidades)
  const responseChannelUserInfoAllChannels = await channelModule.user.getById();
  console.info('responseChannelUserInfoAllChannels =>', responseChannelUserInfoAllChannels);

  //Con esto me trae toda la info de todos los channels del usuario (productos de todos los channels)
  const responseGetAllInfoByUserId = await channelModule.base.getAllInfoByUserId(userGeneralId);
  console.info('getAllInfoByUserId =>', responseGetAllInfoByUserId);

  //Con esto me trae toda la info del channel user en especifico en este caso si quiero mostrar los productos de la tienda del sdk o de wordpress
  // Acá ya seteo el apikey especifico del channel user.

  channelModule.apiKey = apiKeySdk;
  const responseChannelUserInfoSdk = await channelModule.user.getByChannelUserId(
    responseCreateChannelUserSdk.id,
  );
  console.info('responseChannelUserInfoSdk =>', responseChannelUserInfoSdk);

  channelModule.apiKey = apiKeyWordpress;
  const responseChannelUserInfoWordpress = await channelModule.user.getByChannelUserId(
    responseCreateChannelUserWordpress.id,
  );
  console.info('responseChannelUserInfoWordpress =>', responseChannelUserInfoWordpress);

  //Voy agregar 1 producto a cada channel especifico, a tener en cuenta que tambien se le peude enviar collecciones

  channelModule.apiKey = apiKeySdk;
  const responseChannelUserGroupSdk = await channelModule.group.create({
    channel_id: responseCreateChannelUserSdk.channel.id,
    channel_user_id: responseCreateChannelUserSdk.id,
    product_id: '55',
  });
  console.info('responseChannelUserGroupSdk =>', responseChannelUserGroupSdk);

  channelModule.apiKey = apiKeyWordpress;

  const responseChannelUserGroupWordpress = await channelModule.group.create({
    channel_id: responseCreateChannelUserWordpress.channel.id,
    channel_user_id: responseCreateChannelUserWordpress.id,
    product_id: '54',
  });
  console.info('responseChannelUserGroupWordpress =>', responseChannelUserGroupWordpress);

  //Comenzamos con los filtros.
  //acá traemos los items filtrados por channel y la tienda en especifico(channel user)
  channelModule.apiKey = apiKeySdk;
  const responseChannelUserItemGetAllSdk = await channelModule.item.getAll(
    responseCreateChannelUserSdk.channel.id,
    responseCreateChannelUserSdk.id,
  );
  console.info('responseChannelUserItemGetAllSdk =>', responseChannelUserItemGetAllSdk);

  channelModule.apiKey = apiKeyWordpress;
  const responseChannelUserItemGetAllWordpress = await channelModule.item.getAll(
    responseCreateChannelUserWordpress.channel.id,
    responseCreateChannelUserWordpress.id,
  );
  console.info('responseChannelUserItemGetAllWordpress =>', responseChannelUserItemGetAllWordpress);

  //acá traemos los items filtrados por channel y la tienda en especifico(channel user) además del currency
  channelModule.apiKey = apiKeySdk;
  const responseChannelUserItemGetAllCurrencySdk = await channelModule.item.getAllSpecificCurrency(
    responseCreateChannelUserSdk.channel.id,
    'NOK',
    responseCreateChannelUserSdk.id,
  );
  console.info(
    'responseChannelUserItemGetAllCurrencySdk =>',
    responseChannelUserItemGetAllCurrencySdk,
  );

  channelModule.apiKey = apiKeyWordpress;
  const responseChannelUserItemGetAllCurrencyWordpress = await channelModule.item.getAllSpecificCurrency(
    responseCreateChannelUserWordpress.channel.id,
    'NOK',
    responseCreateChannelUserWordpress.id,
  );
  console.info(
    'responseChannelUserItemGetAllCurrencyWordpress =>',
    responseChannelUserItemGetAllCurrencyWordpress,
  );
})();
