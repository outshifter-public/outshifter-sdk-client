import { SdkClient, SdkType } from '@reachu/sdk';

class Api {
  sdkClient: SdkClient;
  constructor() {
    debugger;
    this.sdkClient = new SdkClient(
      'T3CA4EZ-D53MNWZ-MFYJK51-R7ET8EN',
      'https://api-qa.outshifter.com',
    );
  }

  public async createCart(data: SdkType.ShopCart.Cart.Cart) {
    return await this.sdkClient.shopCart.cart.create(data);
  }

  public async getCart(cartId: string) {
    return await this.sdkClient.shopCart.cart.getById(cartId);
  }

  public async createCollection(data: SdkType.Collection.Base.SendCreateService) {
    return await this.sdkClient.collection.base.create(data);
  }

  public async getCollectionByUser(collectionId: string) {
    return await this.sdkClient.collection.base.getById(collectionId);
  }
  public async getInfoByChannelId(channelId: number) {
    return await this.sdkClient.channel.user.getByChannelId(channelId);
  }
}

export default Api;
