import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import ApiService from './api';
import { SdkType } from '@reachu/sdk';

const apiServiceInstance = new ApiService();

function App() {
  const [data, dataSet] = useState<
    (SdkType.Channel.User.SendGetByChannelIdService & Record<string, any>) | null
  >(null);

  const [
    dataCheckout,
    dataCheckoutSet,
  ] = useState<SdkType.ShopCart.Checkout.ResponseUpdateService | null>(null);

  const [
    dataPayment,
    dataPaymentSet,
  ] = useState<SdkType.ShopCart.Payment.ResponseCreateService | null>(null);

  useEffect(() => {
    async function fetchMyAPI() {
      const response: SdkType.Channel.User.SendGetByChannelIdService &
        Record<string, any> = await apiServiceInstance.getInfoByChannelId(2);
      console.info(response.propiedad_que_no_esta_en_el_SDK); // no error
      dataSet(response);
    }

    async function fetchCheckoutAPI() {
      const cartData: SdkType.ShopCart.Cart.SendCreateService = {
        customer_session_id: 'testing_iqplus',
        currency: 'NOK',
      };
      const cart = await apiServiceInstance.sdkClient.shopCart.cart.create(cartData);
      const cartItemData: SdkType.ShopCart.CartItem.SendCreateService = {
        cartId: cart.cart_id,
        line_items: [
          {
            product_id: 4650,
            quantity: 1,
            price_data: {
              unit_price: 369.99,
              tax: 10,
              currency: 'NOK',
            },
          },
          {
            product_id: 4785,
            variant_id: 10350,
            quantity: 1,
            price_data: {
              unit_price: 323.13,
              tax: 10,
              currency: 'NOK',
            },
          },
        ],
      };
      await apiServiceInstance.sdkClient.shopCart.cartItem.create(cartItemData);
      const createCheckoutData: SdkType.ShopCart.Checkout.SendCreateService = {
        cart_id: cart.cart_id,
      };
      const responseCheckoutData = await apiServiceInstance.sdkClient.shopCart.checkout.create(
        createCheckoutData,
      );

      const checkoutData1: SdkType.ShopCart.Checkout.SendUpdateService = {
        success_url:
          'https://test-store-checkout-git-feature-flow-shopcart-iqplus.vercel.app?success_url=646fc99a-8f97-4750-b36a-88c835bcda3d',
        cancel_url:
          'https://test-store-checkout-git-feature-flow-shopcart-iqplus.vercel.app/cart?cancel_url=646fc99a-8f97-4750-b36a-88c835bcda3d',
        email: 'devmiguelopz@gmail.com',
        shipping_address: {
          first_name: 'Miguel',
          last_name: 'test',
          phone: 1234,
          company: 'test',
          address1: 'Molergata 5',
          address2: 'a',
          city: 'Oslo',
          country: 'Norway',
          country_code: 'NOR',
          zip: 180,
        },
        billing_address: {
          first_name: 'Miguel',
          last_name: 'test',
          phone: 1234,
          company: 'test',
          address1: 'Molergata 5',
          address2: 'a',
          city: 'Oslo',
          country: 'Norway',
          country_code: 'NOR',
          zip: 180,
        },
        payment_method: 'stripe',
      };

      await apiServiceInstance.sdkClient.shopCart.checkout.update(
        responseCheckoutData.id,
        checkoutData1,
      );

      const checkoutData2: SdkType.ShopCart.Checkout.SendUpdateService = {
        email: 'devmiguelopz@gmail.com',
        shipping_address: {
          first_name: 'Miguel',
        },
        billing_address: {
          first_name: 'Miguel',
        },
      };

      const responseUpdate = await apiServiceInstance.sdkClient.shopCart.checkout.update(
        responseCheckoutData.id,
        checkoutData2,
      );
      dataCheckoutSet(responseUpdate);

      debugger;
      const paymentData: SdkType.ShopCart.Payment.SendCreateService = {
        success_url: `https://outshifter.atlassian.net?success_url=${responseUpdate.id}`,
        cancel_url: `https://outshifter.atlassian.net/cart?cancel_url=${responseUpdate.id}`,
        email: responseUpdate.email,
        return_url: ``,
        payment_method: 'Stripe',
        first_name: responseUpdate.shipping_address?.first_name as string,
        last_name: responseUpdate.shipping_address?.last_name as string,
        phone: responseUpdate.shipping_address?.phone as number,
        company: responseUpdate.shipping_address?.company as string,
        address1: responseUpdate.shipping_address?.address1 as string,
        address2: responseUpdate.shipping_address?.address2 as string,
        city: responseUpdate.shipping_address?.city as string,
        country: responseUpdate.shipping_address?.country as string,
        country_code: responseUpdate.shipping_address?.country_code as string,
        zip: Number(responseUpdate.shipping_address?.zip as number),
      };

      const response = await apiServiceInstance.sdkClient.shopCart.payment.create(
        responseUpdate.id,
        paymentData,
      );

      dataPaymentSet(response);
    }
    fetchMyAPI();
    fetchCheckoutAPI();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Using!!! <code>src/api/index.ts</code> and save to reload.
        </p>

        <div>
          <h1>Examples Cart - Get Info </h1>
          <code>{data && JSON.stringify(data)}</code>
        </div>

        <div>
          <h1>Examples Cart - Checkout </h1>
          <code>{dataCheckout && JSON.stringify(dataCheckout)}</code>
        </div>

        <div>
          <h1>Examples Cart - Payment </h1>
          <code>{dataPayment && JSON.stringify(dataPayment)}</code>
        </div>
      </header>
    </div>
  );
}

export default App;
