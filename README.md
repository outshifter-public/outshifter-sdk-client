<p align="center">
  <a href="https://www.outshifter.com/" target="blank"><img src="https://images.squarespace-cdn.com/content/v1/565daa97e4b045fcae42b7e6/1593682525596-1J26FDBFZGZRGBKMBJP0/Outshifter.png" width="320" alt="Outshifter Logo" /></a>
</p>

## Description

[reachu](https://gitlab.com/outshifterdev/outshifter-sdk-client) sdk client package starter repository.

## Configuration

- Add token in file .npmrc
```bash
//registry.npmjs.org/:_authToken=${TOKEN}
```
- Use version node of the .nvmrc
```bash
nvm use
```

## Installation

```bash
npm install
yarn install
```
